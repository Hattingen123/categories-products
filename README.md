# categories-products

## Установка проекта
установить: node.js, postgres 10

```
npm init

psql
CREATE DATABASE testdb
\q

set PGUSER=postgres
set PGDATABASE=testdb

node app.js
```

## API
скопировал из поставновки задачи, очень похоже на то, что нужно

### GET /categories Получения списка Категорий
```
200
[
{ id: 1, name: “Food”,
products_count: 9 },
{ id: 2, name: “Drink”,
products_count: 4 }
]
где “products_count” количество Продуктов в Категории, которое должно автоматически увеличиваться или уменьшаться при создании или удалении Продукта.
```
### POST /categories
```
{
category: { name: “Candy” }
}

Создание новой Категории. Атрибут “name” обязательный и должен проверяться на уникальность.

201
{ id: 3, name: “Candy”,
products_count: 0 }
или в случае если
валидация не
прошла
422
{
errors: { name: [“can’t be
blank”]
}
}
```
### GET /categories/ID/products Получение списка Продуктов для Категории
```
200
[
{ id: 1, name: “Beef”, price:
42.69
},
{ id: 2, name: “Cookies”,
price:
69.42 }
]
```
### POST /categories/ID/products
```
Создание нового Продукта в Категории.

{
201
{ id: 2, name: “Butter”, price:
2.6}
product: {
name: “Butter”, price: 2.6

}
Атрибут “name” обязательный и должен проверяться на уникальность. или в случае если валидация не прошла
}
Атрибут “price” обязательный и должен быть больше 0.
{
errors: { name: [“can’t be
blank”]
}
}
```
### DELETE /products/ID Удаление Продукта. 204
{}
