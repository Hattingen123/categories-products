exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.addColumns("products", {
    price: { type: "INTEGER", notNull: true }
  });

  pgm.addConstraint("products", "productsPriceConstraint", {check: 'price > 0 '}); 
};

exports.down = (pgm) => {
  //todo
};
