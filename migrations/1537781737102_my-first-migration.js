exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable("categories", {
  	id: { type: "SERIAL PRIMARY KEY" },
  	name: { type: "VARCHAR", notNull: true},
  	products_count: "INTEGER"
  });

  pgm.addConstraint( "categories", "categoryNameConstraint", {unique: ["name"]} );

  pgm.createTable("products", {
	id: { type: "SERIAL PRIMARY KEY" },
	name: { type: "VARCHAR", notNull: true},
	category: { type: "INTEGER", references: '"categories"'}
  });

  pgm.addConstraint( "products", "productNameConstraint", {unique: ["name"]} );

  pgm.createIndex("products", "category");
};

exports.down = (pgm) => {
  //todo
};
