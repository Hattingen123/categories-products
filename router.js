const express = require('express');
const router = express.Router();
const productController = require('./productController');
const categoryController = require('./categoryController');

router.get('/', function(req, res) {
  res.status(200);
  res.send('this is categories-products API');
});

router.get('/categories', function(req, res) {
  return categoryController.getAllCategories(req, res);
});

router.post('/categories', function(req, res) {
  return categoryController.addCategory(req, res);
});

router.get('/categories/:id/products', function(req, res) {
   return productController.getProductsInCategory(req, res);
});

router.post('/categories/:id/products', function(req, res) {
  return productController.addProduct(req, res);
});

router.delete('/products/:id', function(req, res) {
  return productController.deleteProduct(req, res);
});

module.exports = router;
