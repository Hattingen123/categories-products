const express = require('express');
const productModel = require('./productModel');

exports.getProductsInCategory = function (req, res) {
  const productsPromise = productModel.getProductsInCategory(req.params.id);

  productsPromise.then((rows) => {
  	res.status(200);
    res.send(rows);
  }).catch((err) => {
    throw err;
  });
};

exports.addProduct = function (req, res) {
  const name = req.body.name;
  const category = req.params.id;
  const price = req.body.price;

  const productsPromise = productModel.addProduct(name, category, price);

  productsPromise.then((rows) => {
    res.status(201);
    res.send(rows);
  }).catch(err => {
    let errors = {};
    res.status(500)
    if (err.constraint === 'productsPriceConstraint') {
      errors.name = ["price must be Positive"];
      res.status(422); 
    };
    if (err.constraint === 'productNameConstraint') {
      errors.name = ["name must be unique"];
      res.status(422); 
    };
    if (err.code === '23502') {
      errors.name = ["name and price can't be blank"]
    };
    res.send({errors: errors});
  });
};

exports.deleteProduct = function (req, res) {
  productModel.deleteProduct(req.params.id);
  res.status(204);
  res.send();
};
