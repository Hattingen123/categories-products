const express = require('express');
const categoryModel = require('./categoryModel');

exports.getAllCategories = function (req, res) {
  const categoriesPromise = categoryModel.getAllCategories();

  categoriesPromise.then((rows) => {
  	res.status(200);
    res.send(rows);
  }).catch((err) => {
    res.status(500);
    throw err;
  });
};

exports.addCategory = function (req, res) {
  const categoriesPromise = categoryModel.addCategory(req.body.name);

  categoriesPromise.then((rows) => {
    res.status(201);
    res.send(rows);
  }).catch(err => {
    let errors = {};
    res.status(500);
    if (err.constraint === 'categoryNameConstraint') {
      errors.name = ["name must be unique"];
      res.status(422); 
    };
    if (err.code === '23502') {
      errors.name = ["name can't be blank"]
    };
    res.send({errors: errors});
  });
};
