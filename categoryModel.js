const pg = require('pg');
const connectionString  = 'postgres://localhost:5432/testdb';

const client = new pg.Client({connectionString: connectionString });
const pool = new pg.Pool({connectionString: connectionString });

pool.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err);
  process.exit(-1)
});

exports.getAllCategories = function () {
  return pool.connect().then(client => {
    return client.query("SELECT * FROM categories;").then(res => {
      client.release();
      return res.rows;
    }).catch(e => {
      client.release();
      throw e;
    });
  });
};

exports.addCategory = function (name) {
  return pool.connect().then(client => {
  	return client.query("INSERT INTO categories (name, products_count) VALUES ($1, 0) RETURNING *;", [name]).then(res => {
  	  client.release();
      return res.rows;
  	}).catch(e => {
  	  client.release();
  	  throw e;
  	});
  });
};
