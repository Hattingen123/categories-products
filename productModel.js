const pg = require('pg');
const connectionString  = 'postgres://localhost:5432/testdb';

const client = new pg.Client({connectionString: connectionString });
const pool = new pg.Pool({connectionString: connectionString });

pool.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err);
  process.exit(-1)
});

exports.getProductsInCategory = function (category) {
  return pool.connect().then(client => {
    return client.query("SELECT * FROM products WHERE category = $1;", [category]).then(res => {
      client.release();
      return res.rows;
    }).catch(e => {
      client.release();
      throw e;
    });
  });
};

exports.addProduct = function (name, category, price) {
  return pool.connect().then(client => {
    return client.query("INSERT INTO products (name, category, price) VALUES ($1, $2, $3) RETURNING *;", [name, category, price]).then(res => {
      return client.query("UPDATE categories SET products_count = products_count + 1 WHERE id = $1", [category]).then(() => {
        client.release();
        return res.rows;
      }).catch(e => {
        client.release();
        throw e;
      });
    }); 
  });
};

exports.deleteProduct = function (productID) {
  return pool.connect().then(client => {
  	return client.query("SELECT category FROM products WHERE id = $1", [productID]).then(res => {
  	  return client.query("UPDATE categories SET products_count = products_count - 1 WHERE id = $1", [res.rows[0].category]).then(res => {
	  	return client.query("DELETE FROM products WHERE id=$1;", [productID]).then(res => {
	  	  
	  	  client.release();
	  	}).catch(e => {
	  	  client.release();
	  	  throw e;
	  	});
	  });
  	});
  });
};
