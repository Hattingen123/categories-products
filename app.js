const express = require('express');
const router = require('./router.js');

const app = express();

app.use(express.json());
app.use('/', router);

app.listen(8080, () => console.log('Example app listening on port 8080!'));
